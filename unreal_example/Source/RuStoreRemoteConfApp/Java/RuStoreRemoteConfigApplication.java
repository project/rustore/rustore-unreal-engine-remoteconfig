// Copyright Epic Games, Inc. All Rights Reserved.

package com.example.RuStoreRemoteConfigApp;

import android.util.Log;
import android.content.Context;
import android.content.SharedPreferences;
import com.epicgames.ue4.GameApplication;
import ru.rustore.unitysdk.remoteconfigclient.RuStoreUnityRemoteConfigClient;
import rustore.sdk.unreal.sample.third.R;

public class RuStoreRemoteConfigApplication extends GameApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // Default values from rustore_remoteconf_values.xml
        final String rustore_shared_preferences = getResources().getString(R.string.rustore_shared_preferences);
        final String rustore_app_id_value = getResources().getString(R.string.rustore_app_id);
        final int rustore_update_time_value = getResources().getInteger(R.integer.rustore_update_time);
        final String rustore_update_behaviour_value = getResources().getString(R.string.rustore_update_behaviour);
        final String rustore_account_value = getResources().getString(R.string.rustore_account);
        final String rustore_language_value = getResources().getString(R.string.rustore_language);

        SharedPreferences preferences = getSharedPreferences(rustore_shared_preferences, Context.MODE_PRIVATE);

        String appId = preferences.getString("rustore_app_id", rustore_app_id_value);
        int updateTime = preferences.getInt("rustore_update_time", rustore_update_time_value);
        String updateBehaviour = preferences.getString("rustore_update_behaviour", rustore_update_behaviour_value);
        String account = preferences.getString("rustore_account", rustore_account_value);
        String language = preferences.getString("rustore_language", rustore_language_value);

        RuStoreUnityRemoteConfigClient.INSTANCE.setAccount(account);
        RuStoreUnityRemoteConfigClient.INSTANCE.setLanguage(language);
        RuStoreUnityRemoteConfigClient.INSTANCE.init(appId, updateTime, updateBehaviour, null, null, getApplicationContext());
    }
}
