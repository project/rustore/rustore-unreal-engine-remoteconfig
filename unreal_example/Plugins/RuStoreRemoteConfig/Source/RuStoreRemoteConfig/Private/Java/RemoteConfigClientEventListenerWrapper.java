// Copyright Epic Games, Inc. All Rights Reserved.

package com.Plugins.RuStoreRemoteConfig;

import com.Plugins.RuStoreCore.IRuStoreListener;
import ru.rustore.unitysdk.remoteconfigclient.model.UnityRemoteConfigClientEventListener;

public class RemoteConfigClientEventListenerWrapper implements IRuStoreListener, UnityRemoteConfigClientEventListener
{
    private Object mutex = new Object();
    private long cppPointer = 0;

    private native void NativeOnBackgroundJobErrors(long pointer, Throwable exception);
    private native void NativeOnFirstLoadComplete(long pointer);
    private native void NativeOnInitComplete(long pointer);
    private native void NativeOnMemoryCacheUpdated(long pointer);
    private native void NativeOnPersistentStorageUpdated(long pointer);
    private native void NativeOnRemoteConfigNetworkRequestFailure(long pointer, Throwable throwable);

    public RemoteConfigClientEventListenerWrapper(long cppPointer) {
        this.cppPointer = cppPointer;
    }

    @Override
    public void backgroundJobErrors(Throwable exception) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnBackgroundJobErrors(cppPointer, exception);
            }
        }
    }

    @Override
    public void firstLoadComplete() {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnFirstLoadComplete(cppPointer);
            }
        }
    }

    @Override
    public void initComplete() {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnInitComplete(cppPointer);
            }
        }
    }

    @Override
    public void memoryCacheUpdated() {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnMemoryCacheUpdated(cppPointer);
            }
        }
    }

    @Override
    public void persistentStorageUpdated() {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnPersistentStorageUpdated(cppPointer);
            }
        }
    }

    @Override
    public void remoteConfigNetworkRequestFailure(Throwable throwable) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnRemoteConfigNetworkRequestFailure(cppPointer, throwable);
            }
        }
    }

    public void DisposeCppPointer() {
        synchronized (mutex) {
            cppPointer = 0;
        }
    }
}
