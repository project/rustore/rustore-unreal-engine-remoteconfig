// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStoreConfigRequestParameterProvider.h"
#include "AndroidJavaClass.h"
#include "URuStoreCore.h"

URuStoreConfigRequestParameterProvider* URuStoreConfigRequestParameterProvider::_instance = nullptr;
bool URuStoreConfigRequestParameterProvider::_bIsInstanceInitialized = false;

bool URuStoreConfigRequestParameterProvider::GetIsInitialized()
{
    return bIsInitialized;
}

URuStoreConfigRequestParameterProvider* URuStoreConfigRequestParameterProvider::Instance()
{
    if (!_bIsInstanceInitialized)
    {
        _bIsInstanceInitialized = true;
        _instance = NewObject<URuStoreConfigRequestParameterProvider>(GetTransientPackage());
    }

    return _instance;
}

bool URuStoreConfigRequestParameterProvider::Init()
{
    if (!URuStoreCore::IsPlatformSupported()) return false;
    if (bIsInitialized) return false;

    _instance->AddToRoot();

    auto clientJavaClass = MakeShared<AndroidJavaClass>("ru/rustore/unitysdk/remoteconfigclient/ConfigRequestParameterProviderDefault");
    _clientWrapper = clientJavaClass->GetStaticAJObject("INSTANCE");

    return true;
}

void URuStoreConfigRequestParameterProvider::Dispose()
{
    if (bIsInitialized)
    {
        bIsInitialized = false;
        delete _clientWrapper;
        _instance->RemoveFromRoot();
    }
}

void URuStoreConfigRequestParameterProvider::ConditionalBeginDestroy()
{
    Super::ConditionalBeginDestroy();

    Dispose();
    if (_bIsInstanceInitialized) _bIsInstanceInitialized = false;
}

void URuStoreConfigRequestParameterProvider::SetAccount(FString value)
{
    if (!URuStoreCore::IsPlatformSupported()) return;
    if (bIsInitialized) return;

    _clientWrapper->CallVoid("setAccount", value);
}

FString URuStoreConfigRequestParameterProvider::GetAccount()
{
    if (!URuStoreCore::IsPlatformSupported()) return "";
    if (bIsInitialized) return "";

    return _clientWrapper->CallFString("getAccount");
}

void URuStoreConfigRequestParameterProvider::SetLanguage(FString value)
{
    if (!URuStoreCore::IsPlatformSupported()) return;
    if (bIsInitialized) return;

    _clientWrapper->CallVoid("setLanguage", value);
}

FString URuStoreConfigRequestParameterProvider::GetLanguage()
{
    if (!URuStoreCore::IsPlatformSupported()) return "";
    if (bIsInitialized) return "";

    return _clientWrapper->CallFString("getLanguage");
}
