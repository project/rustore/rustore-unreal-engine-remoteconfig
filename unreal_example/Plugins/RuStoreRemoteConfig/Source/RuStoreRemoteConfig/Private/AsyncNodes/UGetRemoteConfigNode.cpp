// Copyright Epic Games, Inc. All Rights Reserved.

#include "UGetRemoteConfigNode.h"

using namespace RuStoreSDK;

UGetRemoteConfigNode::UGetRemoteConfigNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

UGetRemoteConfigNode* UGetRemoteConfigNode::GetRemoteConfigAsync(URuStoreRemoteConfigClient* target)
{
    auto node = NewObject<UGetRemoteConfigNode>(GetTransientPackage());

    target->GetRemoteConfig(
        [node](long requestId, TSharedPtr<FURuStoreRemoteConfig, ESPMode::ThreadSafe> response) {
            node->Success.Broadcast(*response, FURuStoreError());
        },
        [node](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            node->Failure.Broadcast(FURuStoreRemoteConfig(), *error);
        }
    );

    return node;
}
