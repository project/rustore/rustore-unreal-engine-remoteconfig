// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStoreRemoteConfigClientEventListener.h"
#include "AndroidJavaClass.h"
#include "RemoteConfigClientEventListenerImpl.h"
#include "URuStoreCore.h"

URuStoreRemoteConfigClientEventListener* URuStoreRemoteConfigClientEventListener::_instance = nullptr;
bool URuStoreRemoteConfigClientEventListener::_bIsInstanceInitialized = false;

bool URuStoreRemoteConfigClientEventListener::GetIsInitialized()
{
    return bIsInitialized;
}

URuStoreRemoteConfigClientEventListener* URuStoreRemoteConfigClientEventListener::Instance()
{
    if (!_bIsInstanceInitialized)
    {
        _bIsInstanceInitialized = true;
        _instance = NewObject<URuStoreRemoteConfigClientEventListener>(GetTransientPackage());
    }

    return _instance;
}

bool URuStoreRemoteConfigClientEventListener::Init()
{
    if (!URuStoreCore::IsPlatformSupported()) return false;
    if (bIsInitialized) return false;

    _instance->AddToRoot();

    listener = ListenerBind(new RemoteConfigClientEventListenerImpl(
        [this](long requestId, FString exception) {
            OnBackgroundJobErrors.Broadcast(requestId, exception);
        },
        [this](long requestId) {
            OnFirstLoadComplete.Broadcast(requestId);
        },
        [this](long requestId) {
            OnInitComplete.Broadcast(requestId);
        },
        [this](long requestId) {
            OnMemoryCacheUpdated.Broadcast(requestId);
        },
        [this](long requestId) {
            OnPersistentStorageUpdated.Broadcast(requestId);
        },
        [this](long requestId, FString throwable) {
            OnRemoteConfigNetworkRequestFailure.Broadcast(requestId, throwable);
        }
    ));

    auto clientJavaClass = MakeShared<AndroidJavaClass>("ru/rustore/unitysdk/remoteconfigclient/RemoteConfigClientEventListenerDefault");
    _clientWrapper = clientJavaClass->GetStaticAJObject("INSTANCE");
    _clientWrapper->CallVoid("setListener", listener->GetJWrapper());

    return true;
}

void URuStoreRemoteConfigClientEventListener::Dispose()
{
    if (bIsInitialized)
    {
        bIsInitialized = false;
        ListenerRemoveAll();
        delete _clientWrapper;
        _instance->RemoveFromRoot();
    }
}

void URuStoreRemoteConfigClientEventListener::ConditionalBeginDestroy()
{
    Super::ConditionalBeginDestroy();

    Dispose();
    if (_bIsInstanceInitialized) _bIsInstanceInitialized = false;
}
