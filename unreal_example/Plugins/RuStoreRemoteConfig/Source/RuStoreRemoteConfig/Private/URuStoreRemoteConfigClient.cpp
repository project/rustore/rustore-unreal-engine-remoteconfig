// Copyright Epic Games, Inc. All Rights Reserved.

#include "URuStoreRemoteConfigClient.h"
#include "AndroidJavaClass.h"
#include "URuStoreCore.h"
#include "GetRemoteConfigListenerImpl.h"

using namespace RuStoreSDK;

const FString URuStoreRemoteConfigClient::PluginVersion = "7.0.0";
URuStoreRemoteConfigClient* URuStoreRemoteConfigClient::_instance = nullptr;
bool URuStoreRemoteConfigClient::_bIsInstanceInitialized = false;

bool URuStoreRemoteConfigClient::GetIsInitialized()
{
    return bIsInitialized;
}

URuStoreRemoteConfigClient* URuStoreRemoteConfigClient::Instance()
{
    if (!_bIsInstanceInitialized)
    {
        _bIsInstanceInitialized = true;
        _instance = NewObject<URuStoreRemoteConfigClient>(GetTransientPackage());
    }

    return _instance;
}

bool URuStoreRemoteConfigClient::Init()
{
    if (!URuStoreCore::IsPlatformSupported() || bIsInitialized) return false;

    _instance->AddToRoot();

    URuStoreCore::Instance()->Init();

    auto clientJavaClass = MakeShared<AndroidJavaClass>("ru/rustore/unitysdk/remoteconfigclient/RuStoreUnityRemoteConfigClient");
    _clientWrapper = clientJavaClass->GetStaticAJObject("INSTANCE");
    
    return bIsInitialized = true;
}

void URuStoreRemoteConfigClient::Dispose()
{
    if (bIsInitialized)
    {
        bIsInitialized = false;
        ListenerRemoveAll();
        delete _clientWrapper;
        _instance->RemoveFromRoot();
    }
}

void URuStoreRemoteConfigClient::ConditionalBeginDestroy()
{
    Super::ConditionalBeginDestroy();

    Dispose();
    if (_bIsInstanceInitialized) _bIsInstanceInitialized = false;
}

long URuStoreRemoteConfigClient::GetRemoteConfig(TFunction<void(long, TSharedPtr<FURuStoreRemoteConfig, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure)
{
    if (!URuStoreCore::IsPlatformSupported(onFailure) || !bIsInitialized) return 0;

    auto listener = ListenerBind(new GetRemoteConfigListenerImpl(onSuccess, onFailure, [this](RuStoreListener* item) { ListenerUnbind(item); }));
    _clientWrapper->CallVoid("getRemoteConfig", listener->GetJWrapper());

    return listener->GetId();
}

void URuStoreRemoteConfigClient::GetRemoteConfig(int64& requestId)
{
    requestId = GetRemoteConfig(
        [this](long requestId, TSharedPtr<FURuStoreRemoteConfig, ESPMode::ThreadSafe> response) {
            OnRemoteConfigResponse.Broadcast(requestId, *response);
        },
        [this](long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe> error) {
            OnRemoteConfigError.Broadcast(requestId, *error);
        }
    );
}
