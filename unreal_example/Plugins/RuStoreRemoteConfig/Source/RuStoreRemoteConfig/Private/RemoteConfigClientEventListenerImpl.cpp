// Copyright Epic Games, Inc. All Rights Reserved.

#include "RemoteConfigClientEventListenerImpl.h"
#include "CallbackHandler.h"
#include "URuStoreCore.h"

using namespace RuStoreSDK;

void RemoteConfigClientEventListenerImpl::OnBackgroundJobErrors(AndroidJavaObject* exception)
{
    auto listener = GetWeakPtr();
    CallbackHandler::AddCallback([this, listener]() {
        if (listener.IsValid())
        {
            this->_onBackgroundJobErrors(this->GetId(), "error");
        }
    });
}

void RemoteConfigClientEventListenerImpl::OnFirstLoadComplete()
{
    auto listener = GetWeakPtr();
    CallbackHandler::AddCallback([this, listener]() {
        if (listener.IsValid())
        {
            this->_onFirstLoadComplete(this->GetId());
        }
    });
}

void RemoteConfigClientEventListenerImpl::OnInitComplete()
{
    auto listener = GetWeakPtr();
    CallbackHandler::AddCallback([this, listener]() {
        if (listener.IsValid())
        {
            this->_onInitComplete(this->GetId());
        }
    });
}

void RemoteConfigClientEventListenerImpl::OnMemoryCacheUpdated()
{
    auto listener = GetWeakPtr();
    CallbackHandler::AddCallback([this, listener]() {
        if (listener.IsValid())
        {
            this->_onMemoryCacheUpdated(this->GetId());
        }
    });
}

void RemoteConfigClientEventListenerImpl::OnPersistentStorageUpdated()
{
    auto listener = GetWeakPtr();
    CallbackHandler::AddCallback([this, listener]() {
        if (listener.IsValid())
        {
            this->_onPersistentStorageUpdated(this->GetId());
        }
    });
}

void RemoteConfigClientEventListenerImpl::OnRemoteConfigNetworkRequestFailure(AndroidJavaObject* throwable)
{
    auto listener = GetWeakPtr();
    CallbackHandler::AddCallback([this, listener]() {
        if (listener.IsValid())
        {
            this->_onRemoteConfigNetworkRequestFailure(this->GetId(), "failure");
        }
    });
}

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_com_Plugins_RuStoreRemoteConfig_RemoteConfigClientEventListenerWrapper_NativeOnBackgroundJobErrors(JNIEnv*, jobject, jlong pointer, jobject exception)
    {
        auto obj = new AndroidJavaObject(exception);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<RemoteConfigClientEventListenerImpl*>(pointer);
        castobj->OnBackgroundJobErrors(obj);
    }

    JNIEXPORT void JNICALL Java_com_Plugins_RuStoreRemoteConfig_RemoteConfigClientEventListenerWrapper_NativeOnFirstLoadComplete(JNIEnv*, jobject, jlong pointer)
    {
        auto castobj = reinterpret_cast<RemoteConfigClientEventListenerImpl*>(pointer);
        castobj->OnFirstLoadComplete();
    }

    JNIEXPORT void JNICALL Java_com_Plugins_RuStoreRemoteConfig_RemoteConfigClientEventListenerWrapper_NativeOnInitComplete(JNIEnv*, jobject, jlong pointer)
    {
        auto castobj = reinterpret_cast<RemoteConfigClientEventListenerImpl*>(pointer);
        castobj->OnInitComplete();
    }

    JNIEXPORT void JNICALL Java_com_Plugins_RuStoreRemoteConfig_RemoteConfigClientEventListenerWrapper_NativeOnMemoryCacheUpdated(JNIEnv*, jobject, jlong pointer)
    {
        auto castobj = reinterpret_cast<RemoteConfigClientEventListenerImpl*>(pointer);
        castobj->OnMemoryCacheUpdated();
    }

    JNIEXPORT void JNICALL Java_com_Plugins_RuStoreRemoteConfig_RemoteConfigClientEventListenerWrapper_NativeOnPersistentStorageUpdated(JNIEnv*, jobject, jlong pointer)
    {
        auto castobj = reinterpret_cast<RemoteConfigClientEventListenerImpl*>(pointer);
        castobj->OnPersistentStorageUpdated();
    }

    JNIEXPORT void JNICALL Java_com_Plugins_RuStoreRemoteConfig_RemoteConfigClientEventListenerWrapper_NativeOnRemoteConfigNetworkRequestFailure(JNIEnv*, jobject, jlong pointer, jobject throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<RemoteConfigClientEventListenerImpl*>(pointer);
        castobj->OnRemoteConfigNetworkRequestFailure(obj);
    }
}
#endif
