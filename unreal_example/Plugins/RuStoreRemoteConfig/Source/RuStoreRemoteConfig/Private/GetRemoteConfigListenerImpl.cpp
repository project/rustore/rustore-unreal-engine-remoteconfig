// Copyright Epic Games, Inc. All Rights Reserved.

#include "GetRemoteConfigListenerImpl.h"
#include "AndroidJavaClass.h"

using namespace RuStoreSDK;

GetRemoteConfigListenerImpl::~GetRemoteConfigListenerImpl()
{
}

FURuStoreRemoteConfig* GetRemoteConfigListenerImpl::ConvertResponse(AndroidJavaObject* responseObject)
{
    responseObject->SetInterfaceName("java/lang/Object");
    auto gson = MakeShared<AndroidJavaObject>("com/google/gson/Gson");
    auto jsonString = gson->CallFString("toJson", responseObject);

    auto response = new FURuStoreRemoteConfig();

    TSharedPtr<FJsonObject> jsonObject;
    TSharedRef<TJsonReader<>> jsonReader = TJsonReaderFactory<>::Create(jsonString);
    if (FJsonSerializer::Deserialize(jsonReader, jsonObject) && jsonObject.IsValid())
    {
        const TSharedPtr<FJsonObject>& dataObject = jsonObject->GetObjectField("data");

        for (auto& pair : dataObject->Values)
        {
            FString key = pair.Key;
            FString value;
            pair.Value->TryGetString(value);
            response->data.Add(key, value);
        }
    }

    return response;
}

#if PLATFORM_ANDROID
extern "C"
{
    JNIEXPORT void JNICALL Java_ru_rustore_unitysdk_remoteconfigclient_wrappers_GetRemoteConfigListenerWrapper_NativeOnFailure(JNIEnv* env, jobject, jlong pointer, jthrowable throwable)
    {
        auto obj = new AndroidJavaObject(throwable);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<GetRemoteConfigListenerImpl*>(pointer);
        castobj->OnFailure(obj);
    }

    JNIEXPORT void JNICALL Java_ru_rustore_unitysdk_remoteconfigclient_wrappers_GetRemoteConfigListenerWrapper_NativeOnSuccess(JNIEnv* env, jobject, jlong pointer, jobject result)
    {
        auto obj = new AndroidJavaObject(result);
        obj->UpdateToGlobalRef();

        auto castobj = reinterpret_cast<GetRemoteConfigListenerImpl*>(pointer);
        castobj->OnSuccess(obj);
    }
}
#endif
