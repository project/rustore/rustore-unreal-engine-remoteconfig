// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

class RUSTOREREMOTECONFIG_API URuStoreRemoteConfig
{
public:
	URuStoreRemoteConfig();
	~URuStoreRemoteConfig();
};
