// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStoreUpdateBehaviour.h"
#include "FURuStoreError.h"
#include "FURuStoreRemoteConfig.h"
#include "RuStoreListener.h"
#include "URuStoreRemoteConfigClient.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRuStoreGetRemoteConfigResponseDelegate, int64, requestId, FURuStoreRemoteConfig, response);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRuStoreGetRemoteConfigErrorDelegate, int64, requestId, FURuStoreError, error);

using namespace RuStoreSDK;

/*!
@brief
    Класс реализует API для получения конфигурации данных с удаленного сервера.
    Инкапсулирует запрос конфигурации с сервера, кэширование, фоновое обновление.
*/
UCLASS(Blueprintable)
class RUSTOREREMOTECONFIG_API URuStoreRemoteConfigClient : public UObject, public RuStoreListenerContainer
{
	GENERATED_BODY()

private:
    static URuStoreRemoteConfigClient* _instance;
    static bool _bIsInstanceInitialized;

    bool bIsInitialized = false;
    AndroidJavaObject* _clientWrapper = nullptr;

public:
    /*!
    @brief Версия плагина.
    */
    static const FString PluginVersion;

    /*!
    @brief Проверка инициализации менеджера.
    @return Возвращает true, если синглтон инициализирован, в противном случае — false.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfig Client")
    bool GetIsInitialized();

    /*!
    @brief
       Получить экземпляр URuStoreRemoteConfigClient.
    @return
       Возвращает указатель на единственный экземпляр URuStoreRemoteConfigClient (реализация паттерна Singleton).
       Если экземпляр еще не создан, создает его.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfig Client")
    static URuStoreRemoteConfigClient* Instance();

    /*!
    @brief Выполняет инициализацию синглтона URuStoreRemoteConfigClient.
    @return Возвращает true, если инициализация была успешно выполнена, в противном случае — false.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfig Client")
    bool Init();

    /*!
    @brief Деинициализация синглтона, если дальнейшая работа с объектом больше не планируется.
    */
    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfig Client")
    void Dispose();

    void ConditionalBeginDestroy();

    /*!
    @brief Получение конфигурации данных в зависимости от выбранной политики обновления при инициализации.
    @param onSuccess
        Действие, выполняемое при успешном завершении операции.
        Возвращает requestId типа long и объект FURuStoreRemoteConfig с информцаией о текущем наборе данных.
    @param onFailure
        Действие, выполняемое в случае ошибки.
        Возвращает requestId типа long и объект типа FURuStoreError с информацией об ошибке.
    @return Возвращает уникальный в рамках одного запуска приложения requestId.
    */
    long GetRemoteConfig(TFunction<void(long, TSharedPtr<FURuStoreRemoteConfig, ESPMode::ThreadSafe>)> onSuccess, TFunction<void(long, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure);

    // 
    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfig Client")
    void GetRemoteConfig(int64& requestId);

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfig Client")
    FRuStoreGetRemoteConfigResponseDelegate OnRemoteConfigResponse;

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfig Client")
    FRuStoreGetRemoteConfigErrorDelegate OnRemoteConfigError;
};
