// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "AndroidJavaObject.h"
#include "RuStoreListener.h"
#include "URuStoreRemoteConfigClientEventListener.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRemoteConfigClientEventBackgroundJobErrorsDelegate, int64, requestId, FString, exception);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemoteConfigClientEventFirstLoadCompleteDelegate, int64, requestId);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemoteConfigClientEventInitCompleteDelegate, int64, requestId);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemoteConfigClientEventMemoryCacheUpdatedDelegate, int64, requestId);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemoteConfigClientEventPersistentStorageUpdatedDelegate, int64, requestId);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRemoteConfigClientEventRemoteConfigNetworkRequestFailureDelegate, int64, requestId, FString, throwable);

using namespace RuStoreSDK;

UCLASS(Blueprintable)
class RUSTOREREMOTECONFIG_API URuStoreRemoteConfigClientEventListener : public UObject, public RuStoreListenerContainer
{
	GENERATED_BODY()

private:
    static URuStoreRemoteConfigClientEventListener* _instance;
    static bool _bIsInstanceInitialized;

    bool bIsInitialized = false;
    AndroidJavaObject* _clientWrapper = nullptr;
    TSharedPtr<RuStoreListener, ESPMode::ThreadSafe> listener;

public:
    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfigClientEvent Listener")
    bool GetIsInitialized();

    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfigClientEvent Listener")
    static URuStoreRemoteConfigClientEventListener* Instance();

    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfigClientEvent Listener")
    bool Init();

    UFUNCTION(BlueprintCallable, Category = "RuStore RemoteConfigClientEvent Listener")
    void Dispose();

    void ConditionalBeginDestroy();

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfigClientEvent Listener")
    FRemoteConfigClientEventBackgroundJobErrorsDelegate OnBackgroundJobErrors;

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfigClientEvent Listener")
    FRemoteConfigClientEventFirstLoadCompleteDelegate OnFirstLoadComplete;

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfigClientEvent Listener")
    FRemoteConfigClientEventInitCompleteDelegate OnInitComplete;

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfigClientEvent Listener")
    FRemoteConfigClientEventMemoryCacheUpdatedDelegate OnMemoryCacheUpdated;

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfigClientEvent Listener")
    FRemoteConfigClientEventPersistentStorageUpdatedDelegate OnPersistentStorageUpdated;

    UPROPERTY(BlueprintAssignable, Category = "RuStore RemoteConfigClientEvent Listener")
    FRemoteConfigClientEventRemoteConfigNetworkRequestFailureDelegate OnRemoteConfigNetworkRequestFailure;
};
