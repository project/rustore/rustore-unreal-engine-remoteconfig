// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintAsyncActionBase.h"
#include "URuStoreRemoteConfigClient.h"
#include "UGetRemoteConfigNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGetRemoteConfigPin, FURuStoreRemoteConfig, response, FURuStoreError, error);

UCLASS()
class RUSTOREREMOTECONFIG_API UGetRemoteConfigNode : public UBlueprintAsyncActionBase
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FGetRemoteConfigPin Success;

    UPROPERTY(BlueprintAssignable)
    FGetRemoteConfigPin Failure;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "RuStore RemoteConfig Client")
    static UGetRemoteConfigNode* GetRemoteConfigAsync(URuStoreRemoteConfigClient* target);
};
