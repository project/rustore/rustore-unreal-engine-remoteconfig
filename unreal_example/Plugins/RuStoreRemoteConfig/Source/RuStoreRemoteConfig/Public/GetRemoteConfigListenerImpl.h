// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "URuStoreCore.h"
#include "FURuStoreRemoteConfig.h"
#include "ResponseListener.h"

namespace RuStoreSDK
{
	class RUSTOREREMOTECONFIG_API GetRemoteConfigListenerImpl : public ResponseListener<FURuStoreRemoteConfig>
	{
	protected:
		FURuStoreRemoteConfig* ConvertResponse(AndroidJavaObject* responseObject) override;

	public:
		GetRemoteConfigListenerImpl(
			TFunction<void(long requestId, TSharedPtr<FURuStoreRemoteConfig, ESPMode::ThreadSafe>)> onSuccess,
			TFunction<void(long requestId, TSharedPtr<FURuStoreError, ESPMode::ThreadSafe>)> onFailure,
			TFunction<void(RuStoreListener*)> onFinish
		) : ResponseListener("ru/rustore/unitysdk/remoteconfigclient/wrappers/GetRemoteConfigListenerWrapper", "ru/rustore/unitysdk/remoteconfigclient/callbacks/GetRemoteConfigListener", onSuccess, onFailure, onFinish)
		{
		}

		virtual ~GetRemoteConfigListenerImpl();
	};
}
