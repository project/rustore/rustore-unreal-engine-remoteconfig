// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AndroidJavaObject.h"
#include "ErrorConverter.h"
#include "RuStoreListener.h"

namespace RuStoreSDK
{
    class RUSTOREREMOTECONFIG_API RemoteConfigClientEventListenerImpl : public RuStoreListener
    {
    private:
        TFunction<void(long, FString)> _onBackgroundJobErrors;
        TFunction<void(long)> _onFirstLoadComplete;
        TFunction<void(long)> _onInitComplete;
        TFunction<void(long)> _onMemoryCacheUpdated;
        TFunction<void(long)> _onPersistentStorageUpdated;
        TFunction<void(long, FString)> _onRemoteConfigNetworkRequestFailure;

    public:
        RemoteConfigClientEventListenerImpl(
            TFunction<void(long, FString)> onBackgroundJobErrors,
            TFunction<void(long)> onFirstLoadComplete,
            TFunction<void(long)> onInitComplete,
            TFunction<void(long)> onMemoryCacheUpdated,
            TFunction<void(long)> onPersistentStorageUpdated,
            TFunction<void(long, FString)> onRemoteConfigNetworkRequestFailure
        ) : RuStoreListener("com/Plugins/RuStoreRemoteConfig/RemoteConfigClientEventListenerWrapper", "ru/rustore/unitysdk/remoteconfigclient/model/UnityRemoteConfigClientEventListener")
        {
            _onBackgroundJobErrors = onBackgroundJobErrors;
            _onFirstLoadComplete = onFirstLoadComplete;
            _onInitComplete = onInitComplete;
            _onMemoryCacheUpdated = onMemoryCacheUpdated;
            _onPersistentStorageUpdated = onPersistentStorageUpdated;
            _onRemoteConfigNetworkRequestFailure = onRemoteConfigNetworkRequestFailure;
        }

        virtual ~RemoteConfigClientEventListenerImpl() { }

        void OnBackgroundJobErrors(AndroidJavaObject* exception);
        void OnFirstLoadComplete();
        void OnInitComplete();
        void OnMemoryCacheUpdated();
        void OnPersistentStorageUpdated();
        void OnRemoteConfigNetworkRequestFailure(AndroidJavaObject* throwable);
    };
}
