// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EURuStoreUpdateBehaviour.generated.h"

UENUM(BlueprintType)
enum class EURuStoreUpdateBehaviour : uint8
{
    ACTUAL UMETA(DisplayName = "ACTUAL"),
    DEFAULT UMETA(DisplayName = "DEFAULT"),
    SNAPSHOT UMETA(DisplayName = "SNAPSHOT")
};
