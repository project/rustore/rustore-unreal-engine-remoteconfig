// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "AndroidJavaObject.h"
#include "URuStoreConfigRequestParameterProvider.generated.h"

using namespace RuStoreSDK;

UCLASS(Blueprintable)
class RUSTOREREMOTECONFIG_API URuStoreConfigRequestParameterProvider : public UObject
{
	GENERATED_BODY()

private:
    static URuStoreConfigRequestParameterProvider* _instance;
    static bool _bIsInstanceInitialized;

    bool bIsInitialized = false;
    AndroidJavaObject* _clientWrapper = nullptr;

public:
    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    bool GetIsInitialized();

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    static URuStoreConfigRequestParameterProvider* Instance();

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    bool Init();

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    void Dispose();

    void ConditionalBeginDestroy();

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    void SetAccount(FString value);

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    FString GetAccount();

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    void SetLanguage(FString value);

    UFUNCTION(BlueprintCallable, Category = "RuStore ConfigRequestParameterProvider Listener")
    FString GetLanguage();
};
