// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FURuStoreRemoteConfig.generated.h"

/*!
@brief Текущий набор всех данных, полученных в зависимости от выбранной политики обновлени¤ при инициализации.
*/
USTRUCT(BlueprintType)
struct RUSTOREREMOTECONFIG_API FURuStoreRemoteConfig
{
	GENERATED_USTRUCT_BODY()

public:
	/*!
	@brief Словарь ключ-значение текущего набора данных.
	*/
	UPROPERTY(BlueprintReadOnly)
	TMap<FString, FString> data;
};
