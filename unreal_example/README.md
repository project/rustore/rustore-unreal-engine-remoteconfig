## RuStore Unreal Engine плагин для работы с облачным сервисом конфигурации приложения

### [🔗 Документация разработчика][10]

- [Условия работы SDK](#Условия-работы-sdk)
- [Подготовка требуемых параметров](#Подготовка-требуемых-параметров)
- [Настройка примера приложения](#Настройка-примера-приложения)
- [Сценарий использования](#Сценарий-использования)
- [Условия распространения](#Условия-распространения)
- [Техническая поддержка](#Техническая-поддержка)


### Условия работы SDK

Для работы SDK необходимо соблюдение следующих условий:

1. На устройстве пользователя установлено приложение RuStore.

2. Пользователь авторизован в приложении RuStore.

3. Пользователь и приложение не должны быть заблокированы в RuStore.


### Подготовка требуемых параметров

Для корректной настройки примера приложения необходимо подготовить следующие данные:

1. `applicationId` - уникальный идентификатор приложения в системе Android в формате обратного доменного имени (пример: ru.rustore.sdk.example).

2. `*.keystore` - файл ключа, который используется для [подписи и аутентификации Android приложения](https://www.rustore.ru/help/developers/publishing-and-verifying-apps/app-publication/apk-signature/).

3. `appId` - уникальный идентификатор инструмента remote config. Доступен в [консоли разработчика RuStore](https://console.rustore.ru/toolbox/tools) на странице [создания параметров](https://www.rustore.ru/help/developers/tools/remote-config/parameters) remote config.


###  Настройка примера приложения

1. В настройках проекта (Edit → Project Settings → Platforms → Android) в поле “Android Package Name” укажите `applicationId` - код приложения из консоли разработчика RuStore.

2. В настройках проекта (Edit → Project Settings → Platforms → Android) в разделе “Distribution Signing” укажите расположение и параметры ранее подготовленного файла `*.keystore`.

3. В файле ресурсов _“Source / RuStoreRemoteConfApp / rustore_remoteconf_values.xml”_ в параметре “rustore_app_id” укажите `appId` вашего проекта. AppId можно найти в разделе “Параметры” панели [Remote Config](https://remote-config.rustore.ru/).

4. Выполните сборку проекта и проверьте работу приложения.


### Сценарий использования

#### Выбор модели обновления

Начальный экран приложения предлагает установить значение [UpdateBehaviour][20]. Смена модели обновления потребует перезапуска приложения.

![Выбор модели обновления](images/01_select_update_behaviour.png)


#### Получение конфигурации

Тап по кнопке `Get remote config` выполняет получение и отображение [конфигурации][30].

![Получение конфигурации](images/02_get_remote_config.png)


### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](../MIT-LICENSE.txt).


### Техническая поддержка

Дополнительная помощь и инструкции доступны на странице [rustore.ru/help/](https://www.rustore.ru/help/) и по электронной почте [support@rustore.ru](mailto:support@rustore.ru).

[10]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unreal/7-0-0
[20]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unreal/7-0-0#updatebehaviour
[30]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unreal/7-0-0#getconfig
