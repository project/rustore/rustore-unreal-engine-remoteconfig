## История изменений

### Release 7.0.0
- Версия SDK remoteconfig 7.0.0.

### Release 6.1.0
- Версия SDK remoteconfig 6.1.0.

### Release 1.0
- Версия SDK remoteconfig 1.+.
- Добавлен проект .arr пакетов плагина.

### Release 0.0.2
- Версия SDK remoteconfig 0.0.2.
