package ru.rustore.unitysdk.remoteconfigclient

import ru.rustore.sdk.remoteconfig.RemoteConfigClientEventListener
import ru.rustore.sdk.remoteconfig.RemoteConfigException
import ru.rustore.unitysdk.remoteconfigclient.model.RemoteConfigClientEventListenerEventItem
import ru.rustore.unitysdk.remoteconfigclient.model.UnityRemoteConfigClientEventListener

object RemoteConfigClientEventListenerDefault : RemoteConfigClientEventListener {
    private var listener: UnityRemoteConfigClientEventListener? = null

    private var sequence: Sequence<RemoteConfigClientEventListenerEventItem> = emptySequence()

    @Synchronized
    fun setListener(listener: UnityRemoteConfigClientEventListener) {
        if (this.listener == null && listener == null) return

        this.listener = listener

        sequence.forEachIndexed { _, event ->
            when (event) {
                is RemoteConfigClientEventListenerEventItem.BackgroundJobErrors -> listener.backgroundJobErrors(event.exception)
                is RemoteConfigClientEventListenerEventItem.FirstLoadComplete -> listener.firstLoadComplete()
                is RemoteConfigClientEventListenerEventItem.InitComplete -> listener.initComplete()
                is RemoteConfigClientEventListenerEventItem.MemoryCacheUpdated -> listener.memoryCacheUpdated()
                is RemoteConfigClientEventListenerEventItem.PersistentStorageUpdated -> listener.persistentStorageUpdated()
                is RemoteConfigClientEventListenerEventItem.RemoteConfigNetworkRequestFailure -> listener.remoteConfigNetworkRequestFailure(event.throwable)
            }
        }
        sequence = emptySequence()
    }

    @Synchronized
    override fun backgroundJobErrors(exception: RemoteConfigException.BackgroundConfigUpdateError) {
        listener?.run {
            backgroundJobErrors(exception)
        } ?: run {
            sequence += RemoteConfigClientEventListenerEventItem.BackgroundJobErrors(exception)
        }
    }

    @Synchronized
    override fun firstLoadComplete() {
        listener?.run {
            firstLoadComplete()
        } ?: run {
            sequence += RemoteConfigClientEventListenerEventItem.FirstLoadComplete
        }
    }

    @Synchronized
    override fun initComplete() {
        listener?.run {
            initComplete()
        } ?: run {
            sequence += RemoteConfigClientEventListenerEventItem.InitComplete
        }
    }

    @Synchronized
    override fun memoryCacheUpdated() {
        listener?.run {
            memoryCacheUpdated()
        } ?: run {
            sequence += RemoteConfigClientEventListenerEventItem.MemoryCacheUpdated
        }
    }

    @Synchronized
    override fun persistentStorageUpdated() {
        listener?.run {
            persistentStorageUpdated()
        } ?: run {
            sequence += RemoteConfigClientEventListenerEventItem.PersistentStorageUpdated
        }
    }

    @Synchronized
    override fun remoteConfigNetworkRequestFailure(throwable: Throwable) {
        listener?.run {
            remoteConfigNetworkRequestFailure(throwable)
        } ?: run {
            sequence + RemoteConfigClientEventListenerEventItem.RemoteConfigNetworkRequestFailure(throwable)
        }
    }
}
