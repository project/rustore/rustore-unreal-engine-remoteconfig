package ru.rustore.unitysdk.remoteconfigclient

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObjectNotFoundException

class UITest {
    @Throws(UiObjectNotFoundException::class, InterruptedException::class)
    fun main(args: Array<String>) {
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        Thread.sleep(3000)

        device.click(722, 584)
    }
}
