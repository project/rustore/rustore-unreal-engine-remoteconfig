package ru.rustore.unitysdk.remoteconfigclient.wrappers;

import ru.rustore.sdk.remoteconfig.RemoteConfig;
import ru.rustore.unitysdk.core.IRuStoreListener;
import ru.rustore.unitysdk.remoteconfigclient.callbacks.GetRemoteConfigListener;

public class GetRemoteConfigListenerWrapper implements IRuStoreListener, GetRemoteConfigListener
{
    private Object mutex = new Object();
    private long cppPointer = 0;

    private native void NativeOnFailure(long pointer, Throwable throwable);
    private native void NativeOnSuccess(long pointer, RemoteConfig response);

    public GetRemoteConfigListenerWrapper(long cppPointer) {
        this.cppPointer = cppPointer;
    }

    @Override
    public void OnFailure(Throwable throwable) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnFailure(cppPointer, throwable);
            }
        }
    }

    @Override
    public void OnSuccess(RemoteConfig response) {
        synchronized (mutex) {
            if (cppPointer != 0) {
                NativeOnSuccess(cppPointer, response);
            }
        }
    }

    public void DisposeCppPointer() {
        synchronized (mutex) {
            cppPointer = 0;
        }
    }
}
