package ru.rustore.unitysdk.remoteconfigclient.callbacks;

import ru.rustore.sdk.remoteconfig.RemoteConfig;

public interface GetRemoteConfigListener {
    void OnFailure(Throwable throwable);
    void OnSuccess(RemoteConfig response);
}
