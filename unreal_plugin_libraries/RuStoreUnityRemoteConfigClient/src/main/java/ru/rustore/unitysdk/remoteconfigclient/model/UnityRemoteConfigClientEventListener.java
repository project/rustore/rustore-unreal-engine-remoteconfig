package ru.rustore.unitysdk.remoteconfigclient.model;

public interface UnityRemoteConfigClientEventListener {
    void backgroundJobErrors(Throwable exception);
    void firstLoadComplete();
    void initComplete();
    void memoryCacheUpdated();
    void persistentStorageUpdated();
    void remoteConfigNetworkRequestFailure(Throwable throwable);
}
